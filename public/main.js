//get dom element
let divSelectRoom = document.getElementById('selectRoom');
let divConsultingRoom = document.getElementById('consultingRoom');
let inputRoomNumber = document.getElementById('roomNumber');
let btnGoRoom = document.getElementById('goRoom');
let localVideo = document.getElementById('localVideo');
let remoteVideo = document.getElementById('remoteVideo');

//variables
let roomNumber, localStream, remoteStream, rtcPeerConnection, isCaller ;

const iceServers = {
    'iceServer': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'}
    ]
}

const streamConstraints = { audio: true, video: true }

const socket = io();

btnGoRoom.onclick = () => {
    let invalidRoomNumber = () => {  alert(`please type a room name`) }
    let validRoomNumber = () => {
        roomNumber = inputRoomNumber.value ; 
        socket.emit('create or join', roomNumber);
        divSelectRoom.style = 'display:none';
        divConsultingRoom.style = 'display:block';
    }
    inputRoomNumber.value === '' ?  invalidRoomNumber() : validRoomNumber ();
}

//message handlers
socket.on('created', room => {
    navigator.mediaDevices.getUserMedia(streamConstraints)
    .then(stream => {
        localStream = stream ; 
        localVideo.srcObject = stream ;
        isCaller = true
    })
    .catch(err => {
        console.log('an error ocurred: ', err);
    });
})

socket.on('joined', room => {
    navigator.mediaDevices.getUserMedia(streamConstraints)
    .then(stream => {
        localStream = stream ; 
        localVideo.srcObject = stream ;
        // socket.emit('ready', roomNumber);
        // console.log(`joined: room: ${room}`)
        socket.emit('ready', room);
    })
    .catch(err => {
        console.log('an error ocurred: ', err);
    });
})

let onIceCandidate = (event) => {
    //focus on sending and receiving
    if(event.candidate){
        console.log(`sending ice candidate: `, event.candidate);
        socket.emit('candidate',{
            type:'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate,
            room: roomNumber
        })
    }
}

let onAddStream = (event) => {
    remoteVideo.srcObject = event.streams[0];
    remoteStream = event.streams[0];
}

socket.on('ready', () => {
    if(isCaller){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate; 
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        rtcPeerConnection.createOffer()
        .then( (sessionDescription) => {
            console.log(`sending offer:`, sessionDescription)
            rtcPeerConnection.setLocalDescription(sessionDescription);
            socket.emit('offer', {
                type: 'offer',
                sdp: sessionDescription, 
                room: roomNumber
            })
        })
        .catch( err => {
            console.log(err)
        })
    }
})

socket.on('offer', (event) => {
    if(!isCaller){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate; 
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        // rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(sdp));
        console.log(`received offer:` ,event)
        rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
        rtcPeerConnection.createAnswer()
        .then( (sessionDescription) => {
            console.log(`sending answer: `, sessionDescription)
            rtcPeerConnection.setLocalDescription(sessionDescription);
            socket.emit('answer', {
                type: 'answer',
                sdp: sessionDescription, 
                room: roomNumber
            })
        })
        .catch( err => {
            console.log(err)
        })
    }
})

socket.on('answer', (event) => {
    console.log(`received answer: `, event)
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
})
// socket.on('answer', (sdp) => {
//     rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(sdp));
// })

socket.on('candidate', (event) => {
    const candidate = new RTCIceCandidate({
        sdpMLineIndex: event.label,
        candidate: event.candidate
    });
    console.log(`received candidate`, candidate)
    rtcPeerConnection.addIceCandidate(candidate);
})